def my_func():
    my_list = ['1', '2', 'third', 'four', '5']
    ints = []

    for item in my_list:
        try:
            ints.append(int(item))
        except:
            print('Это не число')
        # except ValueError:
        #     print('Это не число')
        # except Exception:
        #     print('Это еще что такое?')
        # else:
        #     print('Все хорошо')
        # finally:
        #     print(ints)

    # try:
    #     for item in my_list:
    #         ints.append(int(item))
    # except ValueError:
    #     print('Это не число')
    # except Exception:
    #     print('Это еще что такое?')
    # else:
    #     print('Все хорошо')
    # finally:
    #     print(ints)


my_func()


def add_product(units):
    product = input('Что будем покупать? ')
    quantity = float(input('Сколько? '))
    unit = input('1 - кг\n2 - л\n3 - шт\nЧего? ')
    return {'name': product, 'quantity': quantity, 'unit': units[int(unit) - 1]}


def show_product_list(product_list):
    print('Твой список покупок:')
    for i, product in enumerate(product_list):
        print(f"{i + 1}: {product['name']} - {product['quantity']} {product['unit']}")
    print('-----------------------------')


def main():
    units = ('кг', 'л', 'шт')
    command_list = ('add', 'del', 'view', 'edit', 'end')
    product_list = []

    while True:
        my_command = input("1 - Добавить\n2 - Удалить\n3 - Просмотр\n4 - Изменить\n5 - Закончить\nВведите команду: ")
        try:
            command = command_list[int(my_command) - 1]
        except IndexError:
            print('Введите правильную команду из списка')
            continue
        except ValueError:
            print('Команда должна быть числом')
            continue
        else:
            actions = ['Добавляем товар...', 'Удаляем товар...', 'Смотрим список...', 'Изменяем товар...', 'Пока-пока']
            print(actions[int(my_command) - 1])

        if command == 'add':
            try:
                product_list.append(add_product(units))
            except Exception as e:
                print(f'Что-то пошло не так: {e}')
        elif command == 'del':
            try:
                product_index = int(input('Введите номер товара: '))
                product = product_list.pop(product_index - 1)
                print(f"Товар '{product['name']}' удален")
            except Exception as e:
                print(f'Что-то пошло не так: {e}')
        elif command == 'view':
            show_product_list(product_list)
        elif command == 'edit':
            try:
                product_index = int(input('Введите номер товара: '))
                product_list[product_index - 1] = add_product(units)
            except Exception as e:
                print(f'Что-то пошло не так: {e}')
        elif command == 'end':
            show_product_list(product_list)
            break


main()

