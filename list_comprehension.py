greeting = 'hello!'

letter_list = []
for letter in greeting:
    letter_list.append(letter)
print(letter_list)

letter_list = list(greeting)
print(letter_list)

letter_list = [letter.upper() for letter in greeting]
print(letter_list)

number_list = [number for number in '0123456789']
print(number_list)

number_list = [number for number in range(0, 10)]
print(number_list)

number_list = [number ** 2 for number in range(0, 10)]
print(number_list)

number_list = [-((number - 3) / 2 ** 2) for number in range(0, 10)]
print(number_list)

number_list = [6, 43, -2, 11, -55, -12, 3, 345]
new_list = [number ** 3 / 2 for number in number_list if number > 0]
print(new_list)

new_list = ['+' if number > 0 else '-' for number in number_list]
print(number_list)
print(new_list)
