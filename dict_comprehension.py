number_dict = {'first': 1, 'second': 2, 'third': 3}
new_dict = {key: value ** 3 for key, value in number_dict.items()}
print(new_dict)

number_list = [6, 43, -2, 11, -55, -12, 3, 345, 0]
number_dict = {number: number ** 2 for number in number_list}
print(number_dict)

number_dict = {number: '+' if number > 0 else '-' if number < 0 else 'zero' for number in number_list}
print(number_dict)
